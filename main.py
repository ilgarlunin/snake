from PySide import QtGui, QtCore
import gui
import sys
from random import randint, choice
import json
import pygame
from os import path, listdir


pygame.init()
pygame.mixer.init()

HOTKEYS = '''s - begin level\np - pause\nn - next level\n"+"" - sound up\n"-" - sound down\nm - next bg music\nu - bg music play\ny - bg music stop'''

GRID_COLOR = QtGui.QColor(25, 25, 25)
BG_COLOR = QtGui.QColor(100, 100, 100)
DRAW_GRID = True

MUSIC_PATH = "{0}/music/".format(path.dirname(__file__))
SND_PATH = "{0}/sounds/".format(path.dirname(__file__))


class Direction:

    eDirectionUp = "Up"
    eDirectionDown = "Down"
    eDirectionLeft = "Left"
    eDirectionRight = "Right"


class GameState:

    ePlaying = "Playing"
    eFinished = "Finished"
    eReadyToStart = "ReadyToStart"
    ePaused = "Paused"
    eStopped = "Stopped"


class Sound(object):
    """docstring for Sound"""
    def __init__(self):
        super(Sound, self).__init__()
        self.tracks = [MUSIC_PATH+p for p in listdir(MUSIC_PATH)]
        self.SONG_END = pygame.USEREVENT + 1
        pygame.mixer.music.set_endevent(self.SONG_END)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.eventHandler)
        self.timer.start(50)
        pygame.mixer.music.load(self.tracks[0])
        self.shouldStop = False

    def eventHandler(self):
        for event in pygame.event.get():
            if event.type == self.SONG_END:
                pygame.mixer.init()
                pygame.mixer.music.load(self.tracks[randint(0, len(self.tracks)-1)])
                if not self.shouldStop:
                    self.play()

    def next(self):
        self.stop()
        self.play()

    def play(self):
        self.timer.start(50)
        self.shouldStop = False
        pygame.mixer.music.load(self.tracks[randint(0, len(self.tracks)-1)])
        pygame.mixer.music.play()

    def stop(self):
        self.shouldStop = True
        pygame.mixer.music.stop()
        self.timer.stop()

    def sndPause(self):
        snd = pygame.mixer.Sound(SND_PATH+"smb_pause.wav")
        snd.set_volume(pygame.mixer.music.get_volume()+0.1)
        snd.play()

    def sndHit(self):
        snd = pygame.mixer.Sound(SND_PATH+"smb_bump.wav")
        snd.set_volume(pygame.mixer.music.get_volume()+0.1)
        snd.play()


class Location(object):
    """docstring for Location"""
    def __init__(self, row=0, column=0):
        super(Location, self).__init__()
        self.X = row
        self.Y = column

    def __str__(self):
        return "({0}:{1})".format(self.X, self.Y)

    def __eq__(self, other):
        if (self.X == other.X) and (self.Y == other.Y):
            return True
        else:
            return False


class SnakeObjectBase(object):
    """docstring for SnakeObjectBase"""
    def __init__(self, parent, cell_size):
        super(SnakeObjectBase, self).__init__()
        self.parent = parent
        self.grid_location = Location()
        self.cell_size = cell_size
        self.fill_color = (20, 20, 20)
        self.edge_color = (20, 20, 20)

    def setGridPos(self, row, col):
        self.grid_location.X = row
        self.grid_location.Y = col


class FoodBase(SnakeObjectBase):
    """docstring for FoodBase"""
    def __init__(self, parent, cell_size):
        super(FoodBase, self).__init__(parent, cell_size)
        SnakeObjectBase.__init__(self, parent, cell_size)
        self.score_amount = randint(100, 8000)
        self.lifeTime = 3000
        self.fill_color = (20, 20, 20)
        self.edge_color = (55, 55, 55)


class Food(QtGui.QGraphicsRectItem, FoodBase):
    """docstring for Food"""
    def __init__(self, x, y, w, h, parent, scene, step):
        super(Food, self).__init__(x, y, w, h, parent, scene)
        FoodBase.__init__(self, parent, step)
        self.setPen(QtGui.QPen(QtGui.QColor(self.edge_color[0], self.edge_color[1], self.edge_color[2]), 0.5, QtCore.Qt.SolidLine))
        self.setBrush(QtGui.QBrush(QtGui.QColor(self.fill_color[0], self.fill_color[1], self.fill_color[2])))
        self.timer = QtCore.QTimer()
        self.setZValue(2)
        self.timer.timeout.connect(self.jump)
        delay = randint(4000, 7000)
        self.timer.start(delay)
        self.label = QtGui.QGraphicsTextItem(str(delay), self)
        self.label.setPos(0, -20)
        self.labelScore = QtGui.QGraphicsTextItem(str(self.score_amount), self)
        self.labelScore.setPos(0, self.cell_size)

        self.updateTimer = QtCore.QTimer()
        self.updateTimer.timeout.connect(self.updateLabel)
        self.updateTimer.start(20)

        self.elapsedTimer = QtCore.QElapsedTimer()
        self.elapsedTimer.start()

        self.shadow = QtGui.QGraphicsRectItem(x, y, w, h, None, scene)
        self.shadow.setZValue(-1)
        self.shadow.setPen(QtGui.QPen(QtGui.QColor(self.fill_color[0], self.fill_color[1], self.fill_color[2], 120).lighter(80), 0.5, QtCore.Qt.SolidLine))
        self.shadow.setBrush(QtGui.QBrush(QtGui.QColor(self.fill_color[0], self.fill_color[1], self.fill_color[2], 120).lighter(80)))
        self.shadow.MARK_FoodShadow = True

    def pause(self):
        self.timer.stop()
        self.updateTimer.stop()

    def resume(self):
        self.timer.start(self.lifeTime)
        self.elapsedTimer.restart()
        self.updateTimer.start(20)

    def updateLabel(self):
        self.lifeTime = self.timer.interval() - self.elapsedTimer.elapsed()
        self.label.setPlainText(str(self.lifeTime))

    def setGridPos(self, row, col):
        self.setPos(self.cell_size * row, self.cell_size * col)
        SnakeObjectBase.setGridPos(self, row, col)
        self.shadow.setPos(self.pos() + QtCore.QPoint(7, 7))

    def jump(self):
        self.elapsedTimer.restart()
        locs = self.scene().gview.find_good_locations_to_spawn()
        loc = choice(locs)
        self.setGridPos(loc.X, loc.Y)
        self.timer.stop()
        delay = randint(4000, 7000)
        self.timer.start(delay)
        self.label.setPlainText(str(delay))


class Tale(QtGui.QGraphicsRectItem, SnakeObjectBase):
    """docstring for Tale"""
    def __init__(self, x, y, w, h, parent, scene, front, cell_size):
        super(Tale, self).__init__(x, y, w, h, parent, scene)
        SnakeObjectBase.__init__(self, parent, cell_size)
        self.setPen(QtGui.QPen(QtGui.QColor(5, 5, 5), 0.5, QtCore.Qt.SolidLine))
        self.setBrush(QtGui.QBrush(QtGui.QColor(200, 200, 200)))
        self.front = front
        self.back = None
        self.prev_location = Location()

        self.shadow = QtGui.QGraphicsRectItem(x, y, w, h, None, scene)
        self.shadow.setZValue(-1)
        self.shadow.setPen(QtGui.QPen(QtGui.QColor(self.fill_color[0], self.fill_color[1], self.fill_color[2], 120).lighter(80), 0.5, QtCore.Qt.SolidLine))
        self.shadow.setBrush(QtGui.QBrush(QtGui.QColor(self.fill_color[0], self.fill_color[1], self.fill_color[2], 120).lighter(80)))
        self.shadow.MARK_SHADOW = True

    def setGridPos(self, row, col):
        self.setPos(self.cell_size * row, self.cell_size * col)
        self.prev_location = Location(self.grid_location.X, self.grid_location.Y)
        SnakeObjectBase.setGridPos(self, row, col)
        self.shadow.setPos(self.pos() + QtCore.QPoint(7, 7))


class Snake(QtGui.QGraphicsRectItem, SnakeObjectBase):
    """docstring for Food"""
    def __init__(self, x, y, w, h, parent, scene, step, gview):
        super(Snake, self).__init__(x, y, w, h, parent, scene)
        SnakeObjectBase.__init__(self, parent, step)
        self.setPen(QtGui.QPen(QtGui.QColor(5, 5, 5), 0.5, QtCore.Qt.SolidLine))
        self.setBrush(QtGui.QBrush(QtGui.QColor(200, 200, 200)))
        self.gview = gview
        self.body = list()
        self.prev_location = Location()

        self.max_size = 5

        self.arrow = QtGui.QGraphicsLineItem(self.cell_size/2, self.cell_size/2, 0, 0, self, None)
        self.arrow.setPen(QtGui.QColor(255, 0, 0))
        self.arrow_length = 15

        self.fps = 3

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updatePos)

        self.grid_location = Location(self.pos().x()/self.cell_size, self.pos().y()/self.cell_size)
        self.direction = Direction.eDirectionUp
        
        self.shadow = QtGui.QGraphicsRectItem(x, y, w, h, None, scene)
        self.shadow.setZValue(-1)
        self.shadow.setPen(QtGui.QPen(QtGui.QColor(self.fill_color[0], self.fill_color[1], self.fill_color[2], 120).lighter(80), 0.5, QtCore.Qt.SolidLine))
        self.shadow.setBrush(QtGui.QBrush(QtGui.QColor(self.fill_color[0], self.fill_color[1], self.fill_color[2], 120).lighter(80)))
        self.shadow.MARK_SHADOW = True

    def reset(self):
        self.body = list()
        self.prev_location = Location()
        self.direction = Direction.eDirectionUp
        del self.body[:]

    def game_state(self):
        return self.gview.game_state

    def eat(self, food):
        if self.size() == self.max_size-1:
            self.stop()
            self.gview.game_state = GameState.eFinished
            food.pause()
            return False

        tale = Tale(0, 0, float(self.cell_size), float(self.cell_size), None, None, self, self.cell_size)
        self.scene().addItem(tale)
        self.scene().addItem(tale.shadow)
        self.gview.spawnFood()
        if self.body.__len__() == 0:
            tale.front = self
            tale.setGridPos(self.prev_location.X, self.prev_location.Y)
        else:
            tale.front = self.body[-1]
            tale.setGridPos(tale.front.prev_location.X, tale.front.prev_location.Y)
        self.body.append(tale)
        self.gview.updateScore(food.score_amount)

    def stop(self):
        self.timer.stop()

    def start(self):
        if self.timer.isActive():
            self.timer.stop()
        self.timer.start()

    def restart(self, fps):
        self.timer.stop()
        self.timer.start(1000/fps)

    def updatePos(self):
        if self.direction == Direction.eDirectionUp:
            self.up()
        if self.direction == Direction.eDirectionDown:
            self.down()
        if self.direction == Direction.eDirectionLeft:
            self.left()
        if self.direction == Direction.eDirectionRight:
            self.right()

        self.gview.parent.busy = False

    def updateArrow(self):
        if self.direction == Direction.eDirectionUp:
            self.arrow.setLine(self.cell_size/2, self.cell_size/2, self.cell_size/2, self.cell_size/2-self.arrow_length)
            return
        if self.direction == Direction.eDirectionDown:
            self.arrow.setLine(self.cell_size/2, self.cell_size/2, self.cell_size/2, self.cell_size/2+self.arrow_length)
            return
        if self.direction == Direction.eDirectionLeft:
            self.arrow.setLine(self.cell_size/2, self.cell_size/2, self.cell_size/2-self.arrow_length, self.cell_size/2)
            return
        if self.direction == Direction.eDirectionRight:
            self.arrow.setLine(self.cell_size/2, self.cell_size/2, self.cell_size/2+self.arrow_length, self.cell_size/2)
            return

    def move_body(self):
        for i in self.body:
            i.setGridPos(i.front.prev_location.X, i.front.prev_location.Y)

    def size(self):
        return self.body.__len__()


    def setGridPos(self, row, col):

        if not self.game_state() == GameState.ePlaying:
            return False

        x = row
        y = col

        if x in range(0, self.gview.num_columns) and y in range(0, self.gview.num_rows):
            self.prev_location = Location(self.grid_location.X, self.grid_location.Y)
            SnakeObjectBase.setGridPos(self, row, col)
            self.updateArrow()
            self.setPos(self.cell_size * row, self.cell_size * col)
            self.shadow.setPos(self.pos() + QtCore.QPoint(7, 7))

            self.move_body()

            # check if food or body here
            for i in self.scene().items():
                if isinstance(i, FoodBase):
                    if i.grid_location == self.grid_location:
                        self.eat(i)
                if isinstance(i, Tale):
                    if i.grid_location == self.grid_location:
                        self.scene().gview.parent.sound.sndHit()
                        self.scene().gview.restartLevel()
            return True
        else:
            return False

    def up(self):
        if not self.setGridPos(self.grid_location.X, self.grid_location.Y - 1):
            self.setGridPos(self.grid_location.X, self.gview.num_rows-1)

    def left(self):
        if not self.setGridPos(self.grid_location.X - 1, self.grid_location.Y):
            self.setGridPos(self.gview.num_columns-1, self.grid_location.Y)

    def right(self):
        if not self.setGridPos(self.grid_location.X + 1, self.grid_location.Y):
            self.setGridPos(0, self.grid_location.Y)

    def down(self):
        if not self.setGridPos(self.grid_location.X, self.grid_location.Y + 1):
            self.setGridPos(self.grid_location.X, 0)


class Scene(QtGui.QGraphicsScene):
    """docstring for Scene"""
    def __init__(self, parent):
        super(Scene, self).__init__(parent)
        self.gview = parent


class GView(QtGui.QGraphicsView):
    """docstring for GView"""
    def __init__(self, parent):
        super(GView, self).__init__(parent)
        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setScene(Scene(self))
        self.parent = parent
        self.cell_size = 1
        self.num_rows = 0
        self.num_columns = 0
        self.snake = None
        self.isLevelOpened = False
        self._game_state = GameState.eStopped
        self.level_name = str()
        self.score_label = str()
        self.game_state_label = str()
        self.info_label = str()

        self.score = 0

        self.curent_level_description = None

        self.should_draw_grid = True
        self.bg_color = QtGui.QColor(0, 0, 0)
        self.grid_color = QtGui.QColor(255, 255, 255)

    @property
    def game_state(self):
        return self._game_state

    @game_state.setter
    def game_state(self, value):
        self._game_state = value
        try:
            self.game_state_label.setPlainText("Game state:\t{}".format(value))
        except:
            pass

    def find_good_locations_to_spawn(self):
        bad_locations = [self.snake.grid_location] + [t.grid_location for t in self.snake.body]
        good_locations = []
        for x in range(self.num_columns):
            for y in range(self.num_rows):
                if Location(x, y) in bad_locations:
                    continue
                else:
                    good_locations.append(Location(x, y))
        return good_locations

    def restartLevel(self):
        self.closeLevel()
        self.openLevel(self.curent_level_description)

    def startLevel(self, levelDescription):
        if self.game_state == GameState.eFinished:
            self.restartLevel()
        self.snake.restart(int(levelDescription["fps"]))
        self.spawnFood()
        self.game_state = GameState.ePlaying

    def openLevel(self, levelDescription):

        try:
            self.closeLevel()
        except:
            pass

        self.cell_size = int(levelDescription["cell_size"])
        self.setupGrid(int(levelDescription["num_rows"]), int(levelDescription["num_columns"]))
        self.spawnSnake(int(levelDescription["max_snake_len"]))
        self.level_name = QtGui.QGraphicsTextItem("Level:\t{0},\tfps: {1}\tmaxlen: {2}".format(levelDescription["name"], levelDescription["fps"], levelDescription["max_snake_len"]), None, self.scene())
        self.score_label = QtGui.QGraphicsTextItem("Score:\t{0}".format(self.score), None, self.scene())
        self.game_state_label = QtGui.QGraphicsTextItem("Game state:\t_", None, self.scene())
        self.info_label = QtGui.QGraphicsTextItem(HOTKEYS, None, self.scene())
        self.level_name.setPos(0, -40)
        self.score_label.setPos(0, -20)
        self.game_state_label.setPos(0, -60)
        self.info_label.setPos(self.scene().width(), 0)
        self.isLevelOpened = True
        self.game_state = GameState.eReadyToStart
        self.curent_level_description = levelDescription


    def updateScore(self, amount):
        self.score += amount
        self.score_label.setPlainText("Score:\t{}".format(self.score))

    def pause(self):
        self.snake.timer.stop()
        self.game_state = GameState.ePaused
        for i in self.scene().items():
            if isinstance(i, FoodBase):
                i.pause()
        pygame.mixer.music.pause()

    def resume(self):
        if self.game_state == GameState.ePaused:
            self.game_state = GameState.ePlaying
            self.snake.timer.start()
            for i in self.scene().items():
                if isinstance(i, FoodBase):
                    i.resume()
        pygame.mixer.music.unpause()

    def closeLevel(self):
        self.snake.timer.stop()
        self.snake.reset()
        self.scene().clear()
        self.setupGrid(0, 0)
        self.isLevelOpened = False
        self.game_state = GameState.eStopped

    def get_random_location(self):
        return Location(randint(0, self.num_columns-1), randint(0, self.num_rows-1))

    def setupGrid(self, num_rows, num_columns):
        w = self.cell_size * num_columns
        h = self.cell_size * num_rows
        self.scene().setSceneRect(QtCore.QRect(0, 0, w, h))
        self.num_rows = num_rows
        self.num_columns = num_columns

    def spawnSnake(self, max_size):
        loc = self.get_random_location()
        self.snake = Snake(0, 0, float(self.cell_size), float(self.cell_size), None, None, self.cell_size, self)
        self.snake.max_size = max_size
        self.scene().addItem(self.snake)
        self.snake.updateArrow()
        self.snake.setGridPos(loc.X, loc.Y)
        self.scene().addItem(self.snake.shadow)

    def spawnFood(self):
        for i in self.scene().items():
            if hasattr(i, "MARK_FoodShadow"):
                self.scene().removeItem(i)
            if isinstance(i, FoodBase):
                self.scene().removeItem(i)

        locs = self.find_good_locations_to_spawn()
        loc = choice(locs)
        food = Food(0, 0, float(self.cell_size), float(self.cell_size), None, None, self.cell_size)
        self.scene().addItem(food)
        self.scene().addItem(food.shadow)
        food.setGridPos(loc.X, loc.Y)


    def drawBackground(self, painter, rect):
        super(GView, self).drawBackground(painter, rect)

        scene_rect = self.sceneRect()
        try:
            painter.fillRect(rect.intersect(scene_rect), QtGui.QBrush(self.bg_color))
        except:
            painter.fillRect(rect.intersect(scene_rect), QtGui.QBrush(QtGui.QColor(0, 0, 0)))

        if self.should_draw_grid:
            left = int(scene_rect.left()) - (int(scene_rect.left()) % self.cell_size)
            top = int(scene_rect.top()) - (int(scene_rect.top()) % self.cell_size)

            painter.setPen(QtGui.QPen(self.grid_color, 0.5, QtCore.Qt.SolidLine))

            # draw grid vertical lines
            for x in range(left, int(scene_rect.right()), self.cell_size):
                painter.drawLine(x, scene_rect.top() ,x, scene_rect.bottom())

            # draw grid horizontal lines
            for y in range(top, int(scene_rect.bottom()), self.cell_size):
                painter.drawLine(scene_rect.left(), y, scene_rect.right(), y)


class SnakeWindow(QtGui.QMainWindow, gui.Ui_MainWindow):
    """docstring for Window"""
    def __init__(self, levels_path):
        super(SnakeWindow, self).__init__()
        self.setupUi(self)
        self.graphicsView = GView(self)
        self.gridLayout.addWidget(self.graphicsView)
        self.setFocus()
        self.busy = False
        self.current_level = None
        self.level_descriptions = dict()
        self.current_level_id = 0
        self.readLevels(levels_path)
        self.sound = Sound()

    def readLevels(self, path):
        data = None
        with open(path) as f:
            data = json.load(f)
        self.level_descriptions = data
        self.current_level_id = min([int(i) for i in data.keys()])

    def playNextLevel(self):
        level = self.getRandomDescription()
        self.current_level = level
        self.graphicsView.openLevel(level)

    def getRandomDescription(self):
        data = {"cell_size": randint(10, 40), "name": "RandomLevel", "fps": randint(5, 15), "max_snake_len": randint(5, 20), "num_columns": randint(10, 25), "num_rows": randint(10, 25)}
        return data

    def keyPressEvent(self, event):
        key = event.key()

        if key == QtCore.Qt.Key_Up:
            if not self.busy:
                if not self.graphicsView.snake.direction == Direction.eDirectionDown:
                    self.graphicsView.snake.direction = Direction.eDirectionUp
                    self.busy = True

        if key == QtCore.Qt.Key_Left:
            if not self.busy:
                if not self.graphicsView.snake.direction == Direction.eDirectionRight:
                    self.graphicsView.snake.direction = Direction.eDirectionLeft
                    self.busy = True

        if key == QtCore.Qt.Key_Down:
            if not self.busy:
                if not self.graphicsView.snake.direction == Direction.eDirectionUp:
                    self.graphicsView.snake.direction = Direction.eDirectionDown
                    self.busy = True

        if key == QtCore.Qt.Key_Right:
            if not self.busy:
                if not self.graphicsView.snake.direction == Direction.eDirectionLeft:
                    self.graphicsView.snake.direction = Direction.eDirectionRight
                    self.busy = True

        if key == QtCore.Qt.Key_U:
            self.sound.play()

        if key == QtCore.Qt.Key_Y:
            self.sound.stop()

        if key == QtCore.Qt.Key_Plus:
            current = pygame.mixer.music.get_volume()
            pygame.mixer.music.set_volume(current + 0.1)

        if key == QtCore.Qt.Key_Minus:
            current = pygame.mixer.music.get_volume()
            pygame.mixer.music.set_volume(current - 0.1)

        if key == QtCore.Qt.Key_M:
            self.sound.next()

        if key == QtCore.Qt.Key_N:
            self.playNextLevel()
            # try:
            #     self.current_level = self.level_descriptions[str(self.current_level_id)]
            #     self.graphicsView.openLevel(self.current_level)
            #     self.current_level_id += 1
            # except:
            #     print("game over")
            #     self.graphicsView.closeLevel()

        if key == QtCore.Qt.Key_S:
            if self.graphicsView.game_state == GameState.eReadyToStart:
                self.graphicsView.startLevel(self.current_level)
            else:
                print("level not cleared")

        if key == QtCore.Qt.Key_P:
            if self.graphicsView.game_state == GameState.ePaused:
                self.graphicsView.resume()
                super(SnakeWindow, self).keyPressEvent(event)
                self.sound.sndPause()
                return
            if self.graphicsView.game_state == GameState.ePlaying:
                self.graphicsView.pause()
                super(SnakeWindow, self).keyPressEvent(event)
                self.sound.sndPause()
                return

        super(SnakeWindow, self).keyPressEvent(event)


if __name__ == '__main__':

    app = QtGui.QApplication(sys.argv)
    app.setStyle(QtGui.QStyleFactory.create("Cleanlooks"))

    darkPalette = QtGui.QPalette()
    darkPalette.setColor(QtGui.QPalette.Window, QtGui.QColor(53, 53, 53))
    darkPalette.setColor(QtGui.QPalette.WindowText, QtCore.Qt.white)
    darkPalette.setColor(QtGui.QPalette.Base, QtGui.QColor(25, 25, 25))
    darkPalette.setColor(QtGui.QPalette.AlternateBase, QtGui.QColor(53, 53, 53))
    darkPalette.setColor(QtGui.QPalette.ToolTipBase, QtCore.Qt.white)
    darkPalette.setColor(QtGui.QPalette.ToolTipText, QtCore.Qt.white)
    darkPalette.setColor(QtGui.QPalette.Text, QtCore.Qt.white)
    darkPalette.setColor(QtGui.QPalette.Button, QtGui.QColor(53, 53, 53))
    darkPalette.setColor(QtGui.QPalette.ButtonText, QtCore.Qt.white)
    darkPalette.setColor(QtGui.QPalette.BrightText, QtCore.Qt.red)
    darkPalette.setColor(QtGui.QPalette.Link, QtGui.QColor(42, 130, 218))

    darkPalette.setColor(QtGui.QPalette.Highlight, QtGui.QColor(42, 130, 218))
    darkPalette.setColor(QtGui.QPalette.HighlightedText, QtCore.Qt.black)

    app.setPalette(darkPalette)

    app.setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da;+ \
        border: 1px solid white;}\
        QWidget:focus {border:2 inset black;}\
        ")


    window = SnakeWindow(r"D:\GIT\snake\levels\levels.json")
    window.graphicsView.bg_color = BG_COLOR
    window.graphicsView.grid_color = GRID_COLOR
    window.graphicsView.should_draw_grid = DRAW_GRID
    window.show()

    window.playNextLevel()
    window.sound.play()
    pygame.mixer.music.set_volume(0.2)
    sys.exit(app.exec_())
