# My first snake game implementation

![capt.PNG](https://bitbucket.org/repo/g8pzop/images/540676427-capt.PNG)

[Watch video](https://www.youtube.com/watch?v=DRZ_FiUtH4Y)

### Python version:
    - 2.7.13
### Libraries used:
    - PySide
    - Pygame
### Details:
    - Edge teleportation
    - Self collision
    - Food lifetime
    - Object oriented
    - Sound effects and background music
    - Level descriptions
    - Random everything
    - Extendable
    - Endless